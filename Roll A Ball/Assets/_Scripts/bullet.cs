﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bullet : MonoBehaviour
{
    Rigidbody rb;
    public float pushspeed;
    public float lifespan;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        Invoke("Destroy", lifespan);
           
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        rb.velocity = transform.forward * pushspeed;
        
    }
    private void Destroy() {
        Destroy(gameObject);
    }
}
