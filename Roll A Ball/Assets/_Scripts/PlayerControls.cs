﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class PlayerControls : MonoBehaviour
{
    Rigidbody rb;
    int count;
    public float speed;
    public Vector3 size;
    public Text countText;
    public Text winText;
    public GameObject floor;
    float timer;

    private void Update()
    {
        timer += Time.deltaTime;
        print(timer);
    }

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        count = 0;
        SetCountText();
        winText.text = "";
        size = new Vector3 (1f, 1f, 1f);
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        //The following lines grab the current horizontal and vertical axis input for later use
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVeritcal = Input.GetAxis("Vertical");

        // adds the axis input to the current movement
        Vector3 movement = new Vector3(moveHorizontal, 0, moveVeritcal);
        // adds the movement* speed to the force applied to the object
        rb.AddForce(movement * speed);
        rb.transform.localScale = size; 
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Pick Up"))
        {
            other.gameObject.SetActive(false);
            count += 1;
            SetCountText();
            size += new Vector3 (.3f, .3f, .3f); 
        }
    }
    void SetCountText() {
        countText.text = "Count:" + count.ToString();
        if (count > 19)
        {
            
            DeclareVictory();
        }
    }
    void DeclareVictory() {
        winText.text = "You Win!";
    }
}
