﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnBullet : MonoBehaviour
{
    // floats to hold how quickly it 
    public float howRapid;
    public float pLifespan;
    public GameObject pusherPrefab;
    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("createPusher", howRapid, howRapid);
    }

    void createPusher() {
        //instantiate pusher prefab
        GameObject clone = Instantiate(pusherPrefab, transform.position, transform.rotation) as GameObject;
        clone.GetComponent<bullet>().lifespan = pLifespan;
    }
}
